#!/bin/bash
for arg in *; do
    if [[ -d $arg ]]; then
        stow $arg --verbose=2
    fi
done
