let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'dracula/vim', {'as': 'dracula'}
Plug 'preservim/nerdtree'
call plug#end()

syntax on
filetype indent plugin on
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nowrap
set noerrorbells
set number relativenumber
set noswapfile
set nobackup
set wildmenu
set wildmode=full
set incsearch
colorscheme dracula
set background=dark
