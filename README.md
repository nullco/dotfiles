# dotfiles

## Installing dependencies

For now I will list the dependencies here, later on would be nice
to have a script that installs them automatically

* stow (manage dotfiles symlinks)
* arandr (graphical interface for xrandr)
* autorandr (detect monitor configurations automatically depending on connected devices)
* i3
* feh (desktop background)
* picom (window transparency - fading)
* i3blocks (ibar component)
* xclip (for clipboard support in some terminal apps)
* ripgrep
* fd

## Setting up

1. Clone this repo inside user's home directory
2. Execute `bash setup.sh` from this directory
